//
//  Carro.swift
//  classes
//
//  Created by Valdenir Kedon on 25/05/21.
//

import Foundation

class Carro:Veiculo {
    var qtdePortas: Int?
    var cambioAutomatico: Bool?
    
    init(
        qtdeRodas: Int?, cor: String?, qtdePortas: Int?, marca: String?, motor: Float?, combustivel: [String]?, ano: Int?, modelo: String?, cambioAutomatico: Bool?){
        
        super.init()
        self.qtdeRodas = qtdeRodas
        self.cor = cor
        self.qtdePortas = qtdePortas
        self.marca = marca
        self.motor = motor
        self.combustivel = combustivel
        self.ano = ano
        self.modelo = modelo
        self.cambioAutomatico = cambioAutomatico
    }
    
    func imprimirCarro(){
        print(self.qtdeRodas ?? 0)
        print(self.cor ?? "")
        print(self.qtdePortas ?? 0)
        print(self.marca ?? "")
        print(self.motor ?? 0)
        print(self.combustivel ?? "")
        print(self.ano ?? 0)
        print(self.modelo ?? "")
        print(self.cambioAutomatico ?? false)
    }
    
    func cambio()-> String{
        if self.cambioAutomatico ?? false {
            return "Automatico"
        }
        return "Manual"
    }
}
