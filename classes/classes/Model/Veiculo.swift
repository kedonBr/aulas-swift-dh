//
//  Veiculo.swift
//  classes
//
//  Created by Valdenir Kedon on 01/06/21.
//

import Foundation

class Veiculo {
    var qtdeRodas: Int?
    var cor: String?
    var marca: String?
    var motor: Float?
    var combustivel: [String]?
    var ano: Int?
    var modelo: String?
    init(qtdeRodas: Int?,
        cor: String?,
        marca: String?,
        motor: Float?,
        combustivel: [String]?,
        ano: Int?,
        modelo: String?) {
        self.qtdeRodas = qtdeRodas
        self.cor = cor
        self.marca = marca
        self.motor = motor
        self.combustivel = combustivel
        self.ano = ano
        self.modelo = modelo

    }
    init(){
        
    }
    func acelerar(){
        print("acelerar")
    }
    func frear(){
        print("frear")
    }
    func estacionar(){
        print("estacionar")
    }
}
