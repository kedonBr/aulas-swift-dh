//
//  Moto.swift
//  classes
//
//  Created by Valdenir Kedon on 01/06/21.
//

import Foundation

class Moto: Veiculo {
    var cilindradas: Int?
    var tipoFreio: String?
    var guidao: String?
    var esportiva: Bool?
    var pedal: String?
    
    init(
        qtdeRodas: Int?, cor: String?, marca: String?, motor: Float?, combustivel: [String]?, ano: Int?, modelo: String?, cilindradas: Int?, tipoFreio: String?, guidao: String?, esportiva: Bool?, pedal: String?){
        
        super.init(qtdeRodas: qtdeRodas, cor: cor, marca: marca, motor: motor, combustivel: combustivel, ano: ano, modelo: modelo) //super da classe Moto = Veículo
        self.cilindradas = cilindradas
        self.tipoFreio = tipoFreio
        self.guidao = guidao
        self.esportiva = esportiva
        self.pedal = pedal
    }
}
