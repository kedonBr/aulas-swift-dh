//
//  ViewController.swift
//  classes
//
//  Created by Valdenir Kedon on 25/05/21.
//

//var qtdeRodas: Int?
//var cor: String?
//var qtdePortas: Int?
//var marca: String?
//var motor: Float?
//var combustivel: [String]?
//var ano: Int?
//var modelo: String?
//var cambioAutomativo: Bool?

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var corLabel: UILabel!
    @IBOutlet weak var qtdeRoasLabel: UILabel!
    @IBOutlet weak var qtdePortasLabel: UILabel!
    @IBOutlet weak var marcaLabel: UILabel!
    @IBOutlet weak var motorLabel: UILabel!
    @IBOutlet weak var combustivelLabel: UILabel!
    @IBOutlet weak var anoLabel: UILabel!
    @IBOutlet weak var modeloLabel: UILabel!
    @IBOutlet weak var cambioAutomaticoLabel: UILabel!
    @IBOutlet weak var sortear: UIButton!
    var arrayCarros:[Carro] = []    
    var arrayMotos:[Moto] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        var carro1: Carro = Carro(qtdeRodas: 4, cor: "verde", qtdePortas: 4, marca: "VW", motor: 2.0, combustivel: ["alcool", "gasolina"], ano: 2021, modelo: "2022", cambioAutomatico: false)
        
        var carro2: Carro = Carro(qtdeRodas: 4, cor: "verde", qtdePortas: 4, marca: "VW", motor: 2.0, combustivel: ["alcool", "gasolina"], ano: 2022, modelo: "2022", cambioAutomatico: false)
        
        //carro1.modelo = "Palio"
        //carro1.cor = "Azul"
        //carro1.imprimirCarro()
        self.arrayCarros = [carro1, carro2]
        
        var moto1: Moto = Moto(qtdeRodas: 2, cor: "Roxa", marca: "Honda", motor: 2.0, combustivel: ["Gasolina"], ano: 2021, modelo: "1010", cilindradas: 10, tipoFreio: "ABS", guidao: "Tem guidão", esportiva: true, pedal: "Tem")
        
        var moto2: Moto = Moto(qtdeRodas: 2, cor: "Roxa", marca: "Honda", motor: 2.0, combustivel: ["Gasolina"], ano: 2021, modelo: "1010", cilindradas: 10, tipoFreio: "ABS", guidao: "Tem guidão", esportiva: true, pedal: "Tem")
        
        var moto3: Moto = Moto(qtdeRodas: 2, cor: "Roxa", marca: "Honda", motor: 2.0, combustivel: ["Gasolina"], ano: 2021, modelo: "1010", cilindradas: 10, tipoFreio: "ABS", guidao: "Tem guidão", esportiva: true, pedal: "Tem")
        
        var moto4: Moto = Moto(qtdeRodas: 2, cor: "Roxa", marca: "Honda", motor: 2.0, combustivel: ["Gasolina"], ano: 2021, modelo: "1010", cilindradas: 10, tipoFreio: "ABS", guidao: "Tem guidão", esportiva: true, pedal: "Tem")
       
        self.arrayMotos = [moto1,moto2,moto3,moto4]
    }

    @IBAction func botaoSorteio(_ sender: UIButton) {
        if let carroSorteado = self.arrayCarros.randomElement() {
            self.sortearCarro(carroSorteado: carroSorteado)
        }
    }
    
    @IBAction func botaoMoto(_ sender: UIButton) {
        if let motoSorteada = self.arrayMotos.randomElement() {
            self.motoSorteada(motoSorteada: motoSorteada)
        }
    }
    
    func sortearCarro(carroSorteado: Carro){
        
        self.corLabel.backgroundColor = UIColor.green
        self.corLabel.text = carroSorteado.cor
        self.qtdeRoasLabel.text = String(carroSorteado.qtdeRodas ?? 0)
        self.marcaLabel.text = carroSorteado.marca
        self.motorLabel.text = String(carroSorteado.motor ?? 0)
        self.combustivelLabel.text = carroSorteado.combustivel?.first
        self.anoLabel.text = String(carroSorteado.ano ?? 0)
        self.modeloLabel.text = carroSorteado.modelo
        self.cambioAutomaticoLabel.text = carroSorteado.cambio()
    }
    
    func motoSorteada(motoSorteada: Moto){
        
        self.corLabel.backgroundColor = UIColor.green
        self.corLabel.text = motoSorteada.cor
        self.qtdeRoasLabel.text = String(motoSorteada.qtdeRodas ?? 0)
        self.marcaLabel.text = motoSorteada.marca
        self.motorLabel.text = String(motoSorteada.motor ?? 0)
        self.combustivelLabel.text = motoSorteada.combustivel?.first
        self.anoLabel.text = String(motoSorteada.ano ?? 0)
        self.modeloLabel.text = motoSorteada.modelo
    }
    
}

